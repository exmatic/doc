package sugar

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"

	"github.com/pkg/errors"
)

const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type Sugar struct {
	c cipher.Block
}

func NewSugar(pkey []byte) (*Sugar, error) {
	block, err := aes.NewCipher(pkey)
	if err != nil {
		return nil, err
	}

	return &Sugar{
		c: block,
	}, nil
}

func (g *Sugar) GenerateSalt() ([]byte, error) {
	bytes := make([]byte, 32)

	if _, err := rand.Read(bytes); err != nil {
		return nil, err
	}

	for i, b := range bytes {
		bytes[i] = chars[b%byte(len(chars))]
	}

	return bytes, nil
}

func (g *Sugar) Encrypt(data []byte) ([]byte, error) {
	gcm, err := cipher.NewGCM(g.c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, data, nil), nil
}

func (g *Sugar) Decrypt(data []byte) ([]byte, error) {
	gcm, err := cipher.NewGCM(g.c)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	if len(data) < nonceSize {
		return nil, errors.New("invalid length")
	}

	nonce, d := data[:nonceSize], data[nonceSize:]
	res, err := gcm.Open(nil, nonce, d, nil)
	if err != nil {
		return nil, err
	}

	return res, nil
}
