package database

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func addIndex(db *mongo.Database, collName string, indexKeys interface{}) error {
	coll := db.Collection(collName)
	indexName, err := coll.Indexes().CreateOne(context.Background(), mongo.IndexModel{
		Keys:    indexKeys,
		Options: options.Index().SetUnique(true),
	})
	if err != nil {
		return err
	}
	fmt.Println(indexName)
	return nil
}

func OpenMongoConn(dsn string, username string, password string, dbname string) (*mongo.Database, error) {
	ctx := context.Background()
	clientOpts := options.Client().ApplyURI(dsn)
	client, err := mongo.Connect(ctx, clientOpts)
	if err != nil {
		return nil, err
	}

	if err = addIndex(client.Database(dbname), "company-info", bson.M{"company_id": 1}); err != nil {
		return nil, err
	}

	return client.Database(dbname), nil
}

func OpenGridFSBucket(client *mongo.Client, dbname string) (*gridfs.Bucket, error) {
	bucket, err := gridfs.NewBucket(client.Database(dbname))
	if err != nil {
		return nil, err
	}

	return bucket, nil
}
