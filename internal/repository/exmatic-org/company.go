package exmatic_org

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/doc/internal/core/company"
)

var (
	url = "https://exmatic-org.herokuapp.com"
)

type CompanyRepository struct {
	client *http.Client
}

func NewCompanyRepository() company.Repository {
	return &CompanyRepository{
		client: &http.Client{},
	}
}

// FetchByID TODO : Write better error handling
func (r *CompanyRepository) FetchByID(ctx context.Context, companyID string) (*company.Company, error) {
	req, err := http.NewRequest(http.MethodGet, url+"/v1/company/"+companyID, nil)
	if err != nil {
		return nil, err
	}
	tkn := ctx.Value("token").(string)
	if tkn == "" {
		return nil, errors.New("cannot find token in context")
	}

	req.Header.Set("Authorization", "Bearer "+tkn)

	res, err := r.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	var comp company.Company
	if err := json.NewDecoder(res.Body).Decode(&comp); err != nil {
		return nil, err
	}

	return &comp, nil
}
