package exmatic_auth

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/doc/internal/core/user"
)

//TODO : EXPORT ADDR:HOST
var (
	url = "https://exmatic-auth.herokuapp.com"
)

var (
	ErrNoTokensInContext = errors.New("no tokens in context")
)

type UserRepository struct {
	db *http.Client
}

func NewUserRepository(db *http.Client) user.Repository {
	return &UserRepository{
		db: db,
	}
}

func (r *UserRepository) FetchByID(ctx context.Context, id string) (*user.User, error) {
	if id == "" {
		return nil, errors.New("id should not be empty")
	}
	endpoint := url + "/v1/user/" + id

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}

	token, err := tokenFromContext(ctx)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))

	resp, err := r.db.Do(req)
	if err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}
	defer resp.Body.Close()

	var usr user.User
	if err = json.NewDecoder(resp.Body).Decode(&usr); err != nil {
		return nil, fmt.Errorf("updating user info : %w", err)
	}

	return &usr, nil
}

func (r *UserRepository) VerifyTokens(ctx context.Context, token string) (*user.User, error) {
	endpoint := url + "/v1/verify"
	reqData := struct {
		AccessToken string `json:"token"`
	}{
		AccessToken: token,
	}
	data, err := json.Marshal(reqData)
	if err != nil {
		return nil, fmt.Errorf("verifiying tokens : %w", err)
	}

	body := bytes.NewReader(data)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, endpoint, body)
	if err != nil {
		return nil, fmt.Errorf("verifying tokens : %w", err)
	}

	resp, err := r.db.Do(req)
	if err != nil {
		return nil, fmt.Errorf("verifying tokens : %w", err)
	}
	defer resp.Body.Close()

	usr := user.User{}

	if err = json.NewDecoder(resp.Body).Decode(&usr); err != nil {
		return nil, fmt.Errorf("decoding : %w", err)
	}

	return &usr, nil
}

func tokenFromContext(ctx context.Context) (string, error) {
	token := ctx.Value("token").(string)
	if token == "" {
		return "", ErrNoTokensInContext
	}

	return token, nil
}
