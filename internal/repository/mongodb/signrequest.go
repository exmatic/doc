package mongodb

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	"gitlab.com/exmatic/doc/internal/core/signrequest"
)

type SignRequestRepository struct {
	db  *mongo.Collection
	log *zap.SugaredLogger
}

func NewSignRequestRepository(db *mongo.Database, log *zap.SugaredLogger) signrequest.Repository {
	return &SignRequestRepository{
		db: db.Collection("sign-requests"),
	}
}

func (r *SignRequestRepository) Create(ctx context.Context, m *signrequest.Model) error {
	if m == nil {
		return ErrNilDocument
	}
	m.ID = primitive.NewObjectID()

	_, err := r.db.InsertOne(ctx, m)
	if err != nil {
		return err
	}

	return nil
}

func (r *SignRequestRepository) Delete(ctx context.Context, filter bson.D) error {
	_, err := r.db.DeleteOne(ctx, filter)
	if err != nil {
		return fmt.Errorf("deleting sign request : %w", err)
	}

	return nil
}

func (r *SignRequestRepository) Fetch(ctx context.Context, filter bson.D) ([]*signrequest.SignRequest, error) {
	cur, err := r.db.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	var res []*signrequest.SignRequest

	for cur.Next(ctx) {
		if cur.Err() != nil {
			return nil, fmt.Errorf("fetching sign requests : %w", cur.Err())
		}

		var model signrequest.Model
		if err = cur.Decode(&model); err != nil {
			return nil, fmt.Errorf("fetching sign requests : %w", err)
		}

		res = append(res, model.SignRequest())
	}

	return res, nil
}

func (r *SignRequestRepository) FetchOne(ctx context.Context, filter bson.D) (*signrequest.SignRequest, error) {
	res := r.db.FindOne(ctx, filter)
	if res.Err() != nil {
		return nil, fmt.Errorf("fetching sign request : %w", res.Err())
	}

	var model signrequest.Model

	if err := res.Decode(&model); err != nil {
		return nil, fmt.Errorf("fetching sign request : %w", err)
	}

	return model.SignRequest(), nil
}

func (r *SignRequestRepository) Update(ctx context.Context, model *signrequest.Model) error {
	_, err := r.db.UpdateOne(ctx, bson.D{{"_id", model.ID}}, wrapSignRequestModel(model))
	if err != nil {
		return err
	}

	return nil
}

func wrapSignRequestModel(model *signrequest.Model) bson.M {
	res := bson.M{}
	if model.Status != "" {
		res["status"] = model.Status
	}

	return bson.M{"$set": res}
}
