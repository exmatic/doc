package mongodb

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/exmatic/doc/internal/core/document"
)

var (
	ErrNilDocument = errors.New("empty document error")
)

type DocumentRepository struct {
	db *mongo.Collection
}

func NewDocumentRepository(db *mongo.Database) document.Repository {
	return &DocumentRepository{
		db: db.Collection("documents"),
	}
}

func (r *DocumentRepository) Create(ctx context.Context, d *document.Model) error {
	if d == nil {
		return ErrNilDocument
	}

	now := time.Now()

	d.ID = primitive.NewObjectID()
	d.CreatedAt = &now

	_, err := r.db.InsertOne(ctx, d)
	if err != nil {
		return err
	}

	return nil
}

func (r *DocumentRepository) Update(ctx context.Context, update bson.M, id bson.D) error {
	_, err := r.db.UpdateOne(ctx, id, update)
	if err != nil {
		return fmt.Errorf("updating document : %w", err)
	}

	return nil
}

func (r *DocumentRepository) Delete(ctx context.Context, filter bson.D) error {
	_, err := r.db.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	return nil
}

func (r *DocumentRepository) FetchOneByID(ctx context.Context, filter bson.D) (*document.Document, error) {
	res := r.db.FindOne(ctx, filter)
	if res.Err() != nil {
		return nil, fmt.Errorf("fetching document : %w", res.Err())
	}

	var model document.Model

	if err := res.Decode(&model); err != nil {
		return nil, fmt.Errorf("fetching document : %w", err)
	}

	return model.Document(), nil
}

func (r *DocumentRepository) Fetch(ctx context.Context, filter bson.D) ([]*document.Document, error) {
	cur, err := r.db.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	var res []*document.Document

	for cur.Next(ctx) {
		if cur.Err() != nil {
			return nil, fmt.Errorf("fetching document : %w", cur.Err())
		}

		var model document.Model
		if err = cur.Decode(&model); err != nil {
			return nil, fmt.Errorf("fetching document : %w", err)
		}

		res = append(res, model.Document())
	}

	return res, nil
}

/*func wrapModel(doc *document.Model) (bson.M, error) {
	res := bson.M{}
	if doc == nil {
		return nil, errors.New("empty update form")
	}

	if doc.Name != nil {
		res["name"] = doc.Name
	}
	if doc.Type != "" {
		res["type"] = doc.Type
	}

	if doc.File != nil {
		res["file"] = doc.File
	}

	if doc.Companies != nil {
		res["companies"] = doc.Companies
	}

	return res, nil
}

func wrapTransactions(doc *document.Model) (bson.M, error) {
	res := bson.M{}
	if doc.Transactions == nil {
		return nil, errors.New("empty transactions")
	}

	if doc.SignatureHash != nil {
		res["signature_hash"] = doc.SignatureHash
	}

	if doc.Companies != nil {
		res["companies"] = doc.Companies
	}

	res["transactions"] = doc.Transactions

	return bson.M{"$set": res}, nil
}
*/
