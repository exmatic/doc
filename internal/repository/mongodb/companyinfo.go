package mongodb

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"

	"gitlab.com/exmatic/doc/internal/core/companyinfo"
)

type CompanyInfoRepository struct {
	db  *mongo.Collection
	log *zap.SugaredLogger
}

func NewCompanyInfoRepository(db *mongo.Database, logger *zap.SugaredLogger) companyinfo.Repository {
	return &CompanyInfoRepository{
		db:  db.Collection("company-info"),
		log: logger,
	}
}

func (r *CompanyInfoRepository) Create(ctx context.Context, model *companyinfo.Model) error {
	res, err := r.db.InsertOne(ctx, model)
	if err != nil {
		return fmt.Errorf("inserting company info : %w", err)
	}

	r.log.Info("inserted company info: ", res.InsertedID, model.CompanyID)
	return nil
}

func (r *CompanyInfoRepository) FetchOne(ctx context.Context, filter bson.D) (*companyinfo.CompanyInfo, error) {
	res := r.db.FindOne(ctx, filter)
	if res.Err() != nil {
		return nil, fmt.Errorf("fetching company info : %w", res.Err())
	}

	var model companyinfo.Model

	if err := res.Decode(&model); err != nil {
		return nil, fmt.Errorf("fetching company info : %w", err)
	}

	return model.CompanyInfo(), nil
}
