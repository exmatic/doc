package mongodb

import (
	"bytes"
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/gridfs"

	"gitlab.com/exmatic/doc/internal/core/file"
)

type FileRepository struct {
	bucket *gridfs.Bucket
}

func NewFileRepository(bucket *gridfs.Bucket) file.Repository {
	return &FileRepository{
		bucket: bucket,
	}
}

func (r *FileRepository) Create(ctx context.Context, name string, file []byte, id primitive.ObjectID) error {
	err := r.bucket.UploadFromStreamWithID(id, name, bytes.NewBuffer(file))
	if err != nil {
		return err
	}

	return nil
}

func (r *FileRepository) Delete(ctx context.Context, id primitive.ObjectID) error {
	if err := r.bucket.Delete(id); err != nil {
		return err
	}
	return nil
}

func (r *FileRepository) FetchOne(ctx context.Context, id primitive.ObjectID) ([]byte, error) {
	var buff bytes.Buffer
	if _, err := r.bucket.DownloadToStream(id, &buff); err != nil {
		return nil, err
	}

	return buff.Bytes(), nil
}
