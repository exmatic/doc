package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/doc/internal/core/role"
	"gitlab.com/exmatic/doc/internal/core/user"
	"gitlab.com/exmatic/doc/internal/web/v1"
	"gitlab.com/exmatic/doc/pkg/web"
)

func Authorize(userRole ...role.Role) web.Middleware {
	m := func(handler web.Handler) web.Handler {
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

			// If the context is missing this value return failure.
			usr := ctx.Value("user").(*user.User)
			if usr == nil {
				return v1.NewRequestError(fmt.Errorf("you are not authorized for the action, no user data found"), http.StatusForbidden)
			}

			if !isMatched(usr.Role, userRole...) {
				return v1.NewRequestError(
					fmt.Errorf("you are not authorized for that action, claims[%v] roles[%v]", usr.Role, userRole),
					http.StatusForbidden,
				)
			}

			return handler(ctx, w, r)
		}

		return h
	}

	return m
}

func isMatched(target role.Role, roles ...role.Role) bool {
	for _, r := range roles {
		if target == r {
			return true
		}
	}

	return false
}

func Authenticate(a user.Auth) web.Middleware {
	m := func(handler web.Handler) web.Handler {
		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
			// Expecting: bearer <token>
			accessBearer := r.Header.Get("authorization")
			// Parse the authorization header.

			accessParts := strings.Split(accessBearer, " ")
			if len(accessParts) != 2 || strings.ToLower(accessParts[0]) != "bearer" {
				err := errors.New("expected authorization header format: bearer <token>")
				return v1.NewRequestError(err, http.StatusUnauthorized)
			}

			accessToken := accessParts[1]

			// Validate the token is signed by us.
			usr, err := a.VerifyTokens(ctx, accessToken)

			if err != nil {
				return v1.NewRequestError(err, http.StatusUnauthorized)
			}

			// Add claims to the context, so they can be retrieved later.
			ctx = context.WithValue(ctx, "user", usr)
			ctx = context.WithValue(ctx, "token", accessToken)

			// Call the next handler.
			return handler(ctx, w, r)
		}

		return h
	}

	return m
}
