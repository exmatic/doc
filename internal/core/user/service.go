package user

import "context"

type Service interface {
	FetchOneByID(ctx context.Context, id string) (*User, error)
}

type Auth interface {
	VerifyTokens(ctx context.Context, tokens string) (*User, error)
}
