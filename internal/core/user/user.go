package user

import (
	"time"

	"gitlab.com/exmatic/doc/internal/core/role"
)

type User struct {
	ID        string    `json:"id"`
	Profile   Profile   `json:"profile"`
	Info      *Info     `json:"info"`
	Role      role.Role `json:"role"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type Profile struct {
	Email     string    `json:"email" bson:"email"`
	Username  string    `json:"username" bson:"username"`
	Firstname string    `json:"firstname" bson:"firstname"`
	Lastname  string    `json:"lastname" bson:"lastname"`
	BirthDate time.Time `json:"birthDate" bson:"birthDate"`
	Password  string    `json:"password" bson:"password"`
}

type Info struct {
	CompanyID          string    `json:"company_id"`
	DepartmentID       string    `json:"department_id"`
	ProjectID          string    `json:"project_id"`
	TeamID             string    `json:"team_id"`
	PositionID         string    `json:"position_id"`
	GradeID            string    `json:"grade_id"`
	Salary             int       `json:"salary"`
	ActiveVacationDays int       `json:"active_vacation_days"`
	IsActive           int       `json:"is_active"`
	Skills             []string  `json:"skills"`
	Responsibilities   []string  `json:"responsibilities"`
	WorkingSince       time.Time `json:"date_working_since"`
}
