package user

import (
	"context"
)

type Repository interface {
	VerifyTokens(ctx context.Context, token string) (*User, error)
	FetchByID(ctx context.Context, id string) (*User, error)
}
