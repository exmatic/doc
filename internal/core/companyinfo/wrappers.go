package companyinfo

import "github.com/google/uuid"

func (m *Model) CompanyInfo() *CompanyInfo {
	var res CompanyInfo
	if m.CompanyID != uuid.Nil {
		res.CompanyID = m.CompanyID.String()
	}

	if m.PrivateKey != nil {
		res.PrivateKey = *m.PrivateKey
	}

	return &res
}
