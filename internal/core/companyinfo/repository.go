package companyinfo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

type Repository interface {
	Create(ctx context.Context, model *Model) error
	FetchOne(ctx context.Context, filter bson.D) (*CompanyInfo, error)
}
