package companyinfo

import "context"

type Service interface {
	Create(ctx context.Context, info *CompanyInfo) error
	FetchOne(ctx context.Context, companyID string) (*Raw, error)
}
