package companyinfo

import "github.com/google/uuid"

//CompanyInfo stores hashedprivate key and sugar for security
type CompanyInfo struct {
	CompanyID  string `json:"company_id"`
	PrivateKey string `json:"private_key"`
	//Salt       string `json:"sugar"`
}

type Raw struct {
	CompanyID  string `json:"company_id"`
	PrivateKey []byte `json:"private_key"`
	//Salt       []byte
}

type Model struct {
	CompanyID  uuid.UUID `bson:"company_id"`
	PrivateKey *string   `bson:"private_key"`
	//Add salt
	//Salt       *string   `bson:"salt"`

}
