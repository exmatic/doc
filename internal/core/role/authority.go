package role

type Role string

var (
	Admin    Role = "ROLE_ADMIN"
	User     Role = "ROLE_USER"
	HR       Role = "ROLE_HR"
	Employee Role = "ROLE_EMPLOYEE"
)
