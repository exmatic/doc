package signrequest

import (
	"context"

	"gitlab.com/exmatic/doc/internal/core/user"
)

type Service interface {
	Create(ctx context.Context, req *New, user *user.User) error

	Fetch(ctx context.Context, filter *Filter, user *user.User) ([]*SignRequest, error)
	FetchOneByID(ctx context.Context, id string, user *user.User) (*SignRequest, error)
	Decline(ctx context.Context, id string, user *user.User) error

	Sign(ctx context.Context, id string, user *user.User) error

	Delete(ctx context.Context, id string, user *user.User) error
}

type Filter struct {
	IDs      []string
	Types    []string
	Statuses []string
	UserID   string
	Outer    bool
}
