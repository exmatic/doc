package signrequest

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

type Repository interface {
	Create(ctx context.Context, model *Model) error
	Delete(ctx context.Context, filter bson.D) error
	Fetch(ctx context.Context, filter bson.D) ([]*SignRequest, error)
	FetchOne(ctx context.Context, filter bson.D) (*SignRequest, error)
	Update(ctx context.Context, model *Model) error
}
