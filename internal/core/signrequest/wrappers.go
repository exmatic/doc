package signrequest

func (m *Model) SignRequest() *SignRequest {

	res := &SignRequest{
		ID:        m.ID.Hex(),
		Receivers: m.Receivers,
	}

	if m.Type != "" {
		res.Type = m.Type
	}
	if m.Status != "" {
		res.Status = m.Status
	}
	if m.Description != nil {
		res.Description = *m.Description
	}
	if m.DocumentID != nil {
		res.DocumentID = *m.DocumentID
	}
	if m.SenderID != nil {
		res.SenderID = *m.SenderID
	}

	if m.CreatedAt != nil {
		res.CreatedAt = *m.CreatedAt
	}

	return res
}
