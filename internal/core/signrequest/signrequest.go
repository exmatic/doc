package signrequest

import (
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Status string

var (
	Declined        Status = "STATUS_DECLINED"
	Completed       Status = "STATUS_COMPLETED"
	InProcess       Status = "STATUS_IN_PROCESS"
	DocumentDeleted Status = "STATUS_DOCUMENT_DELETED"
)

type Type string

var (
	Inner Type = "REQUEST_TYPE_INNER"
	Outer Type = "REQUEST_TYPE_OUTER"
)

type SignRequest struct {
	ID          string    `json:"id"`
	Type        Type      `json:"type" swaggertype:"string" example:"REQUEST_TYPE_INNER"`
	Status      Status    `json:"status" swaggertype:"string" example:"STATUS_IN_PROCESS"`
	SenderID    string    `json:"sender_id"`
	DocumentID  string    `json:"document_id"`
	Description string    `json:"description"`
	Receivers   []string  `json:"users"`
	CreatedAt   time.Time `json:"created_at"`
}

type Model struct {
	ID          primitive.ObjectID `json:"_id" bson:"_id"`
	Type        Type               `json:"type" bson:"type"`
	Status      Status             `json:"status" bson:"status"`
	SenderID    *string            `json:"sender_id" bson:"sender_id"`
	DocumentID  *string            `json:"document_id" bson:"document_id"`
	Description *string            `json:"description" bson:"description"`
	Receivers   []string           `json:"receivers" bson:"receivers"`
	CreatedAt   *time.Time         `json:"created_at" bson:"created_at"`
}

type New struct {
	Type        Type     `json:"type" swaggertype:"string" example:"REQUEST_TYPE_INNER"`
	DocumentID  string   `json:"document_id"`
	Description string   `json:"description"`
	Receivers   []string `json:"receivers"`
}

func (n *New) Validate() error {
	if n.Type == "" || n.DocumentID == "" || n.Receivers == nil || n.Description == "" {
		return errors.New("required fields cannot be empty")
	}
	if n.Type != Inner && n.Type != Outer {
		return errors.New("invalid sign request type")
	}

	return nil
}
