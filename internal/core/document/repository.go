package document

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
)

type Repository interface {
	Create(ctx context.Context, document *Model) error
	Update(ctx context.Context, update bson.M, id bson.D) error
	Delete(ctx context.Context, filter bson.D) error
	FetchOneByID(ctx context.Context, filter bson.D) (*Document, error)
	Fetch(ctx context.Context, filter bson.D) ([]*Document, error)
}
