package document

type Transaction struct {
	CompanyID  string    `json:"company_id"`
	DocumentID string    `json:"document_id"`
	Signature  Signature `json:"signature"`
}
