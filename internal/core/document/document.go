package document

import (
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Type string

var (
	CV             Type = "DOC_TYPE_CV"
	MedicalRecord  Type = "DOC_TYPE_MEDICAL_RECORD"
	Diploma        Type = "DOC_TYPE_DIPLOMA"
	MilitaryRecord Type = "DOC_TYPE_MILITARY_RECORD"
	PersonalID     Type = "DOC_TYPE_PERSONAL_ID"
	Contract       Type = "DOC_TYPE_CONTRACT"
)

//Document contains base64(url) encoded string of file
type Document struct {
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	Type        Type       `json:"type" swaggertype:"string" example:"DOC_TYPE_CONTRACT"`
	Description string     `json:"description"`
	UserID      string     `json:"user_id"`
	CreatorID   string     `json:"creator_id"`
	FileID      string     `json:"file_id"`
	Info        *Info      `json:"info"`
	CreatedAt   time.Time  `json:"created_at"`
	UpdatedAt   *time.Time `json:"updated_at,omitempty"`
}

type Info struct {
	Companies     []string      `json:"companies" bson:"companies"`
	CertificateID *string       `json:"certificate_id" bson:"certificate_id"`
	SignatureHash *string       `json:"signature_hash" bson:"signature_hash"`
	Transactions  []Transaction `json:"transactions,omitempty" bson:"transactions"`
}

//Model contains raw byte data of file
type Model struct {
	ID          primitive.ObjectID `json:"_id" bson:"_id"`
	Name        *string            `json:"name" bson:"name"`
	Type        Type               `json:"type" bson:"type"`
	UserID      *string            `json:"user_id" bson:"user_id"`
	Description *string            `json:"description" bson:"description"`
	//CreatorID creator's company id
	CreatorID *string            `json:"creator_id" bson:"creator_id"`
	FileID    primitive.ObjectID `json:"file_id" bson:"file_id"`
	Info      *Info              `json:"info" bson:"info"`
	CreatedAt *time.Time         `json:"created_at" bson:"created_at"`
	UpdatedAt *time.Time         `json:"updated_at" bson:"updated_at"`
}

// UpdateInfo Updating document info
type UpdateInfo struct {
	SignatureHash *string       `json:"signature_hash" bson:"signature_hash"`
	Transactions  []Transaction `json:"transactions" bson:"transactions"`
	Companies     []string      `json:"companies" bson:"companies"`
}

type Update struct {
	Name        string `json:"name"`
	Type        Type   `json:"type" swaggertype:"string" example:"DOC_TYPE_CONTRACT"`
	Description string `json:"description"`
}

func (u *Update) Validate() error {
	if u.Type != "" {
		switch u.Type {
		case CV, MedicalRecord, Diploma, MilitaryRecord, PersonalID, Contract:
			return nil
		default:
			return ErrInvalidDocumentType
		}
	}

	return nil
}

var (
	ErrNewDocumentValidation = errors.New("some fields are empty")
	ErrInvalidDocumentType   = errors.New("invalid document type")
)

// New Document update form
type New struct {
	Name        string `json:"name"`
	Type        Type   `json:"type" swaggertype:"string" example:"DOC_TYPE_CONTRACT"`
	Description string `json:"description"`
	File        string `json:"file" format:"base64" example:"aGVsbG8="`
}

func (n *New) Validate() error {
	if n.Name == "" || n.Type == "" || n.File == "" {
		return ErrNewDocumentValidation
	}

	switch n.Type {
	case CV, MedicalRecord, Diploma, MilitaryRecord, PersonalID, Contract:
		return nil
	default:
		return ErrInvalidDocumentType
	}
}
