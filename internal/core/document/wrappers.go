package document

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (m *Model) Document() *Document {
	var res Document

	res.ID = m.ID.Hex()

	if m.Name != nil {
		res.Name = *m.Name
	}

	if m.Description != nil {
		res.Description = *m.Description
	}

	if m.FileID != primitive.NilObjectID {
		res.FileID = m.FileID.Hex()
	}

	if m.CreatorID != nil {
		res.CreatorID = *m.CreatorID
	}

	if m.UserID != nil {
		res.UserID = *m.UserID
	}

	if m.Type != "" {
		res.Type = m.Type
	}

	if m.Info != nil {
		res.Info = &Info{}
		if m.Info.SignatureHash != nil {
			res.Info.SignatureHash = m.Info.SignatureHash
		}

		if m.Info.CertificateID != nil {
			res.Info.CertificateID = m.Info.CertificateID
		}

		res.Info.Transactions = m.Info.Transactions
		res.Info.Companies = m.Info.Companies
	}

	if m.CreatedAt != nil {
		res.CreatedAt = *m.CreatedAt
	}

	if m.UpdatedAt != nil {
		res.UpdatedAt = m.UpdatedAt
	}

	return &res
}
