package document

import "time"

type Signature struct {
	UserID    string    `json:"user_id"`
	Hash      []byte    `json:"hash"`
	Timestamp time.Time `json:"timestamp"`
}
