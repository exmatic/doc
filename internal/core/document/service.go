package document

import (
	"context"

	"gitlab.com/exmatic/doc/internal/core/file"
	"gitlab.com/exmatic/doc/internal/core/user"
)

type Service interface {
	Create(ctx context.Context, document *New, user *user.User) error //without updates
	UpdateInfo(ctx context.Context, document *UpdateInfo, documentID string) error
	Update(ctx context.Context, document *Update, documentID string, user *user.User) error
	FetchOne(ctx context.Context, id string, user *user.User) (*Document, error)
	Fetch(ctx context.Context, filters Filter, user *user.User) ([]*Document, error)
	Delete(ctx context.Context, id string, user *user.User) error
	FetchRaw(ctx context.Context, id string, user *user.User) (*file.File, error)
	UpdateCertID(ctx context.Context, documentID string, fileID string) error
	DeleteInfo(ctx context.Context, documentID string) error
	FetchOneByIDUnlocked(ctx context.Context, id string) (*Document, error)
}

type CertificateService interface {
	Create(ctx context.Context, document *Document) ([]byte, error)
}

type Filter struct {
	IDs       []string
	Types     []string
	Companies []string
	Users     []string
	Outer     bool
}
