package file

type File struct {
	ID          string `json:"id"`
	Data        string `json:"data"`
	Certificate string `json:"certificate"`
}
