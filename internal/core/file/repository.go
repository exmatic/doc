package file

import (
	"context"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Repository interface {
	Create(ctx context.Context, name string, file []byte, id primitive.ObjectID) error
	Delete(ctx context.Context, id primitive.ObjectID) error
	FetchOne(ctx context.Context, id primitive.ObjectID) ([]byte, error)
}
