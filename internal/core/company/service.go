package company

import "context"

type Service interface {
	FetchByID(ctx context.Context, id string) (*Company, error)
}
