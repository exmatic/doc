package company

import "time"

type Company struct {
	ID             string    `json:"id"`
	OwnerID        string    `json:"owner_id"`
	Name           string    `json:"name"`
	Phone          string    `json:"phone"`
	Email          string    `json:"email"`
	Address        string    `json:"address,omitempty"`
	BIK            string    `json:"bik,omitempty"`
	Website        string    `json:"website,omitempty"`
	DateCreated    time.Time `json:"date_created"`
	DateRegistered time.Time `json:"date_registered"`
}
