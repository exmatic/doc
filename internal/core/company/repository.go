package company

import "context"

type Repository interface {
	FetchByID(ctx context.Context, id string) (*Company, error)
}
