package service

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/doc/internal/core/signrequest"
)

func wrapSignRequestFilter(filter *signrequest.Filter) bson.D {
	if filter == nil {
		return bson.D{}
	}

	res := bson.A{}

	if filter.IDs != nil {
		arr := bson.A{}
		for _, v := range filter.IDs {
			id, err := primitive.ObjectIDFromHex(v)
			if err != nil {
				continue
			}
			arr = append(arr, bson.D{{"_id", id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}
	if filter.Types != nil {
		arr := bson.A{}
		for _, v := range filter.Types {
			arr = append(arr, bson.D{{"type", v}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}

	if filter.Statuses != nil {
		arr := bson.A{}
		for _, v := range filter.Statuses {
			arr = append(arr, bson.D{{"status", v}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}

	if filter.UserID != "" {
		var comp bson.D
		if filter.Outer {
			comp = bson.D{{"receivers", filter.UserID}}
		} else {
			comp = bson.D{{"sender_id", filter.UserID}}
		}

		res = append(res, comp)
	}

	result := bson.D{{"$and", res}}

	return result
}
