package service

import (
	"context"
	"encoding/base64"
	"fmt"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/exmatic/doc/internal/core/companyinfo"
	"gitlab.com/exmatic/doc/pkg/sugar"
)

var (
	ErrNilCompanyInfo               = errors.New("company info is nil")
	ErrInvalidCompanyID             = errors.New("invalid company_id")
	ErrUnableToGenerateRandomString = errors.New("unable to generate random string")
	ErrInvalidSizeErr               = errors.New("WHAT THE FUCK")
)

type CompanyInfoService struct {
	repository companyinfo.Repository
	s          sugar.Sugar
}

func NewCompanyInfoService(repository companyinfo.Repository, sugar *sugar.Sugar) companyinfo.Service {
	return &CompanyInfoService{
		repository: repository,
		s:          *sugar,
	}
}

/*
Create
Algorithm :
	pkey: generating random bytes
	salt: generating random bytes

	Base64 Encoding pkey, salt
	AES Encrypting base64(pkey) + "." + base64(salt)
	Base64 Encoding Encrypted Data
*/
func (s *CompanyInfoService) Create(ctx context.Context, info *companyinfo.CompanyInfo) error {
	if info == nil {
		return ErrNilCompanyInfo
	}

	encoder := base64.RawURLEncoding

	id, err := uuid.Parse(info.CompanyID)
	if err != nil {
		return ErrInvalidCompanyID
	}

	var model companyinfo.Model
	model.CompanyID = id

	pkeyBytes, err := s.s.GenerateSalt()
	if err != nil {
		return err
	}
	if pkeyBytes == nil {
		fmt.Println("SUKAAAAA BYTES ERROR")
	}

	encrypted, err := s.s.Encrypt(pkeyBytes)
	if err != nil {
		return err
	}
	//encoded and encrypted
	enc := encoder.EncodeToString(encrypted)
	model.PrivateKey = &enc

	if err := s.repository.Create(ctx, &model); err != nil {
		return err
	}

	return nil
}

func (s *CompanyInfoService) FetchOne(ctx context.Context, companyID string) (*companyinfo.Raw, error) {
	id, err := uuid.Parse(companyID)
	if err != nil {
		return nil, ErrInvalidCompanyID
	}

	encoder := base64.RawURLEncoding

	info, err := s.repository.FetchOne(ctx, bson.D{{"company_id", id}})
	if err != nil {
		return nil, err
	}

	encryptedBytes, err := encoder.DecodeString(info.PrivateKey)
	if err != nil {
		return nil, err
	}

	data, err := s.s.Decrypt(encryptedBytes)

	if err != nil {
		return nil, err
	}

	res := companyinfo.Raw{
		CompanyID:  info.CompanyID,
		PrivateKey: data,
	}

	return &res, nil
}
