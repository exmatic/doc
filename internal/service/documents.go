package service

import (
	"context"
	"encoding/base64"
	"log"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/doc/internal/core/document"
	"gitlab.com/exmatic/doc/internal/core/file"
	"gitlab.com/exmatic/doc/internal/core/role"
	"gitlab.com/exmatic/doc/internal/core/user"
)

var (
	ErrInvalidDocumentID = errors.New("invalid document id")
	ErrNoDocumentData    = errors.New("document is nil")
	ErrInvalidFile       = errors.New("invalid file")
	ErrDocumentNotFound  = errors.New("document not found")
	ErrUserForbidden     = errors.New("cannot manage document that was created by user with higher role")
	ErrNilUserInfo       = errors.New("user not in a company")
)

type DocumentService struct {
	repo     document.Repository
	fileRepo file.Repository
	userRepo user.Repository
}

func NewDocumentService(repository document.Repository, fileRepository file.Repository, userRepo user.Repository) document.Service {
	return &DocumentService{
		repo:     repository,
		fileRepo: fileRepository,
		userRepo: userRepo,
	}
}

func (s *DocumentService) Create(ctx context.Context, doc *document.New, usr *user.User) error {
	if doc == nil {
		return ErrNoDocumentData
	}

	if err := doc.Validate(); err != nil {
		return err
	}

	enc := base64.StdEncoding

	//Decoding base64 string
	bytes, err := enc.DecodeString(doc.File)
	if err != nil {
		return ErrInvalidFile
	}

	fileID := primitive.NewObjectID()

	if err = s.fileRepo.Create(ctx, doc.Name, bytes, fileID); err != nil {
		return err
	}

	model := &document.Model{
		Name:        &doc.Name,
		Type:        doc.Type,
		UserID:      &usr.ID,
		Description: &doc.Description,
		CreatorID:   &usr.Info.CompanyID,
		FileID:      fileID,
	}

	if err = s.repo.Create(ctx, model); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) Update(ctx context.Context, upd *document.Update, documentID string, usr *user.User) error {
	docID, err := primitive.ObjectIDFromHex(documentID)
	if err != nil {
		return ErrInvalidDocumentID
	}

	doc, err := s.repo.FetchOneByID(ctx, bson.D{{"_id", docID}})
	if err != nil {
		return err
	}

	if usr.ID != doc.UserID {
		if err = s.checkHierarchy(ctx, usr.Role, doc.UserID); err != nil {
			return err
		}
	}

	if doc.Type == document.Contract && doc.Info != nil {
		return errors.New("cannot update document => type: contract && signInfo is not empty")
	}

	if err = s.repo.Update(ctx, wrapDocumentModel(upd), bson.D{{"_id", docID}}); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) UpdateInfo(ctx context.Context, doc *document.UpdateInfo, documentID string) error {
	if doc == nil {
		return errors.New("nil document model")
	}

	docID, err := primitive.ObjectIDFromHex(documentID)
	if err != nil {
		return ErrInvalidDocumentID
	}

	docu, err := s.repo.FetchOneByID(ctx, bson.D{{"_id", docID}})
	if err != nil {
		return err
	}

	if docu.Info == nil {
		upd := bson.M{
			"$set": bson.M{
				"info": &document.Info{},
			},
		}
		if err = s.repo.Update(ctx, upd, bson.D{{"_id", docID}}); err != nil {
			return err
		}
	}

	if err = s.repo.Update(ctx, wrapDocumentInfoModel(doc), bson.D{{"_id", docID}}); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) FetchOne(ctx context.Context, id string, usr *user.User) (*document.Document, error) {
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, ErrInvalidDocumentID
	}

	doc, err := s.repo.FetchOneByID(ctx, bson.D{{"_id", docID}})
	if err != nil {
		return nil, ErrDocumentNotFound
	}

	if doc.CreatorID != usr.Info.CompanyID {
		if doc.Info == nil || (doc.Info != nil && !checkCompany(doc.Info.Companies, usr.Info.CompanyID)) {
			log.Println(doc.Info.Companies, " ", usr.Info.CompanyID)
			return nil, ErrDocumentNotFound
		}
	}

	return doc, nil
}

func (s *DocumentService) Fetch(ctx context.Context, filter document.Filter, usr *user.User) ([]*document.Document, error) {
	filter.Companies = []string{usr.Info.CompanyID}

	res, err := s.repo.Fetch(ctx, wrapDocumentFilter(&filter))
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (s *DocumentService) UpdateCertID(ctx context.Context, documentID string, fileID string) error {
	docID, err := primitive.ObjectIDFromHex(documentID)
	if err != nil {
		return errors.New("invalid document id")
	}
	newID, err := primitive.ObjectIDFromHex(fileID)
	if err != nil {
		return errors.New("invalid file id")
	}

	u := bson.M{
		"$set": bson.M{
			"info.certificate_id": newID,
		},
	}

	if err = s.repo.Update(ctx, u, bson.D{{"_id", docID}}); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) DeleteInfo(ctx context.Context, documentID string) error {
	id, err := primitive.ObjectIDFromHex(documentID)
	if err != nil {
		return err
	}

	if err := s.repo.Update(ctx, bson.M{"$set": bson.M{
		"info": nil,
	}}, bson.D{{"_id", id}}); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) FetchOneByIDUnlocked(ctx context.Context, id string) (*document.Document, error) {
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, ErrDocumentNotFound
	}

	doc, err := s.repo.FetchOneByID(ctx, bson.D{{"_id", docID}})
	if err != nil {
		return nil, err
	}

	return doc, nil
}

func (s *DocumentService) Delete(ctx context.Context, id string, usr *user.User) error {
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return ErrInvalidDocumentID
	}

	doc, err := s.repo.FetchOneByID(ctx, bson.D{{"_id", docID}, {"creator_id", usr.Info.CompanyID}})
	if err != nil {
		return err
	}

	if usr.ID != doc.UserID {
		if err = s.checkHierarchy(ctx, usr.Role, doc.UserID); err != nil {
			return err
		}
	}

	fileID, err := primitive.ObjectIDFromHex(doc.FileID)

	if err = s.fileRepo.Delete(ctx, fileID); err != nil {
		return err
	}

	if err = s.repo.Delete(ctx, bson.D{{"_id", docID}}); err != nil {
		return err
	}

	return nil
}

func (s *DocumentService) FetchRaw(ctx context.Context, id string, user *user.User) (*file.File, error) {
	doc, err := s.FetchOne(ctx, id, user)
	if err != nil {
		return nil, err
	}

	fileID, err := primitive.ObjectIDFromHex(doc.FileID)
	if err != nil {
		return nil, err
	}

	raw, err := s.fileRepo.FetchOne(ctx, fileID)
	if err != nil {
		return nil, err
	}

	enc := base64.StdEncoding

	var certData string
	if doc.Info != nil && doc.Info.CertificateID != nil {
		certID, err := primitive.ObjectIDFromHex(*doc.Info.CertificateID)
		if err != nil {
			return nil, err
		}
		certRaw, _ := s.fileRepo.FetchOne(ctx, certID)
		certData = enc.EncodeToString(certRaw)
	}

	return &file.File{
		ID:          doc.ID,
		Data:        enc.EncodeToString(raw),
		Certificate: certData,
	}, nil
}

func checkCompany(arr []string, el string) bool {
	for _, v := range arr {
		if v == el {
			return true
		}
	}

	return false
}

func (s *DocumentService) checkFetchHierarchy(ctx context.Context, authUserRole role.Role, docUserId string) error {
	usr, err := s.userRepo.FetchByID(ctx, docUserId)
	if err != nil {
		return err
	}

	if usr.Info == nil {
		return ErrNilUserInfo
	}

	if authUserRole == role.Employee && (usr.Role == role.HR || usr.Role == role.Admin) {
		return ErrUserForbidden
	}

	return nil
}

func (s *DocumentService) checkHierarchy(ctx context.Context, authUserRole role.Role, docUserId string) error {
	usr, err := s.userRepo.FetchByID(ctx, docUserId)
	if err != nil {
		return err
	}

	if usr.Info == nil {
		return ErrNilUserInfo
	}

	if (authUserRole == role.HR && usr.Role == role.Admin) ||
		(authUserRole == role.Employee && (usr.Role == role.HR || usr.Role == role.Admin)) {
		return ErrUserForbidden
	}

	return nil
}
