package service

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/doc/internal/core/document"
)

func wrapDocumentModel(m *document.Update) bson.M {
	res := bson.M{}

	if m.Name != "" {
		res["name"] = m.Name
	}
	if m.Type != "" {
		res["type"] = m.Type
	}
	if m.Description != "" {
		res["description"] = m.Description
	}

	return bson.M{"$set": res}
}

func wrapDocumentInfoModel(m *document.UpdateInfo) bson.M {
	res := bson.M{}

	if m.SignatureHash != nil {
		res["info.signature_hash"] = m.SignatureHash
	}
	if m.Transactions != nil {
		res["info.transactions"] = m.Transactions
	}
	if m.Companies != nil {
		res["info.companies"] = m.Companies
	}

	return bson.M{"$set": res}
}

func wrapDocumentFilter(filter *document.Filter) bson.D {
	if filter == nil {
		return nil
	}

	res := bson.A{}

	if filter.IDs != nil {
		arr := bson.A{}
		for _, v := range filter.IDs {
			id, err := primitive.ObjectIDFromHex(v)
			if err != nil {
				continue
			}
			arr = append(arr, bson.D{{"_id", id}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}

	if filter.Types != nil {
		arr := bson.A{}
		for _, v := range filter.Types {
			arr = append(arr, bson.D{{"type", v}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}

	if filter.Companies != nil {
		var comp bson.D
		if filter.Outer {
			comp = bson.D{{"info.companies", filter.Companies[0]}}
		} else {
			comp = bson.D{{"creator_id", filter.Companies[0]}}
		}

		res = append(res, comp)
	}

	if filter.Users != nil {
		arr := bson.A{}
		for _, v := range filter.Users {
			_, err := primitive.ObjectIDFromHex(v)
			if err != nil {
				continue
			}
			arr = append(arr, bson.D{{"user_id", v}})
		}

		res = append(res, bson.D{
			{"$or", arr},
		})
	}

	result := bson.D{{"$and", res}}

	return result

}
