package service

import (
	"context"

	"gitlab.com/exmatic/doc/internal/core/company"
)

type CompanyService struct {
	repo company.Repository
}

func NewCompanyService(repo company.Repository) company.Service {
	return &CompanyService{
		repo: repo,
	}
}

func (s *CompanyService) FetchByID(ctx context.Context, companyID string) (*company.Company, error) {
	return s.repo.FetchByID(ctx, companyID)
}
