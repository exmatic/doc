package service

import (
	"context"

	"gitlab.com/exmatic/doc/internal/core/user"
)

type Auth struct {
	repo user.Repository
}

func NewAuth(userRepo user.Repository) user.Auth {
	return &Auth{
		repo: userRepo,
	}
}

func (a *Auth) VerifyTokens(ctx context.Context, token string) (*user.User, error) {
	return a.repo.VerifyTokens(ctx, token)
}
