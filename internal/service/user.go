package service

import (
	"context"

	"gitlab.com/exmatic/doc/internal/core/user"
)

type UserService struct {
	repo user.Repository
}

func NewUserService(repository user.Repository) user.Service {
	return &UserService{
		repo: repository,
	}
}

func (s *UserService) FetchOneByID(ctx context.Context, id string) (*user.User, error) {
	return s.repo.FetchByID(ctx, id)
}
