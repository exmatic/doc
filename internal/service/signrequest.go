package service

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"log"
	"time"

	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/exmatic/doc/internal/core/companyinfo"
	"gitlab.com/exmatic/doc/internal/core/document"
	"gitlab.com/exmatic/doc/internal/core/file"
	"gitlab.com/exmatic/doc/internal/core/signrequest"
	"gitlab.com/exmatic/doc/internal/core/user"
)

var (
	ErrForbidden            = errors.New("access denied")
	ErrInvalidSignRequestID = errors.New("invalid sign request id")
	ErrSignRequestNotFound  = errors.New("sign request not found")
)

type SignRequestService struct {
	repo               signrequest.Repository
	companyInfoService companyinfo.Service
	userRepo           user.Repository
	documentService    document.Service
	fileRepo           file.Repository
	certService        document.CertificateService
}

func NewSignRequestService(repo signrequest.Repository, companyInfoService companyinfo.Service, documentService document.Service, fileRepo file.Repository, userRepo user.Repository, certService document.CertificateService) signrequest.Service {
	return &SignRequestService{
		repo:               repo,
		companyInfoService: companyInfoService,
		documentService:    documentService,
		fileRepo:           fileRepo,
		userRepo:           userRepo,
		certService:        certService,
	}
}

func (s *SignRequestService) Create(ctx context.Context, req *signrequest.New, usr *user.User) error {
	if err := req.Validate(); err != nil {
		return err
	}

	if req.Type == signrequest.Inner {
		for _, id := range req.Receivers {
			usr1, err := s.userRepo.FetchByID(ctx, id)
			if err != nil {
				return err
			}
			if usr1.Info == nil {
				return errors.New("user must be in company to sign the document")
			}

			if usr1.Info.CompanyID != usr.Info.CompanyID {
				return errors.New("users from another companies cannot be included in inner sign-request")
			}
		}
	}

	doc, err := s.documentService.FetchOneByIDUnlocked(ctx, req.DocumentID)
	if err != nil {
		return err
	}

	if doc.Type != document.Contract {
		return errors.New("only contracts can be signed")
	}

	var companies []string

	for _, rec := range req.Receivers {
		usr, err := s.userRepo.FetchByID(ctx, rec)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		companies = appendWithoutDuplicates(companies, usr.Info.CompanyID)
	}

	if err := s.documentService.UpdateInfo(ctx, &document.UpdateInfo{
		Companies: companies,
	}, req.DocumentID); err != nil {
		return err
	}

	now := time.Now()
	model := &signrequest.Model{
		Type:        req.Type,
		Status:      signrequest.InProcess,
		SenderID:    &usr.ID,
		DocumentID:  &req.DocumentID,
		Receivers:   req.Receivers,
		Description: &req.Description,
		CreatedAt:   &now,
	}

	if err := s.repo.Create(ctx, model); err != nil {
		return err
	}

	return nil
}

// Fetch TODO :WRITE BETTER FILTRATION
func (s *SignRequestService) Fetch(ctx context.Context, filter *signrequest.Filter, usr *user.User) ([]*signrequest.SignRequest, error) {
	/*	filter := bson.M{
			"$or": bson.A{
				bson.M{"receivers": usr.ID},
				bson.M{"sender_id": usr.ID},
			},
		}
	*/

	filter.UserID = usr.ID
	requests, err := s.repo.Fetch(ctx, wrapSignRequestFilter(filter))
	if err != nil {
		return nil, ErrSignRequestNotFound
	}

	return requests, nil
}

func (s *SignRequestService) FetchOneByID(ctx context.Context, id string, usr *user.User) (*signrequest.SignRequest, error) {
	requestID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, ErrInvalidSignRequestID
	}

	filter := bson.D{
		{"$and", bson.A{
			bson.D{{"_id", requestID}},
			bson.D{{"$or", bson.A{
				bson.M{"receivers": usr.ID},
				bson.M{"sender_id": usr.ID},
			}},
			}},
		},
	}

	res, err := s.repo.FetchOne(ctx, filter)
	if err != nil {
		return nil, ErrSignRequestNotFound
	}

	return res, nil
}

func (s *SignRequestService) Decline(ctx context.Context, id string, usr *user.User) error {
	requestID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return ErrInvalidSignRequestID
	}

	req, err := s.repo.FetchOne(ctx, bson.D{{"_id", requestID}})
	if err != nil {
		return ErrSignRequestNotFound
	}

	if req.Status == signrequest.Declined {
		return nil
	}

	model := &signrequest.Model{
		ID:     requestID,
		Status: signrequest.Declined,
	}

	if err = s.repo.Update(ctx, model); err != nil {
		return err
	}

	return nil
}

func (s *SignRequestService) Sign(ctx context.Context, id string, usr *user.User) error {
	req, err := s.FetchOneByID(ctx, id, usr)
	if err != nil {
		return err
	}

	//checking status whether the sign request is already completed
	if req.Status == signrequest.Completed {
		return errors.New("sign request already completed")
	}

	if req.Status == signrequest.Declined {
		return errors.New("someone declined sign request, unable to sign")
	}

	if req.SenderID != usr.ID && !checkUser(req.Receivers, usr.ID) {
		return errors.New("you are not allowed to sign the document")
	}

	doc, err := s.documentService.FetchOne(ctx, req.DocumentID, usr)
	if err != nil {
		return err
	}

	//Checking for transaction whether the user already signed the document
	if verifySignature(doc.Info.Transactions, usr.ID) {
		return errors.New("already signed")
	}

	comp, err := s.companyInfoService.FetchOne(ctx, usr.Info.CompanyID)
	if err != nil {
		return err
	}

	encoder := base64.StdEncoding

	fileID, err := primitive.ObjectIDFromHex(doc.FileID)
	if err != nil {
		return err
	}

	rawFile, err := s.fileRepo.FetchOne(ctx, fileID)
	strFile := encoder.EncodeToString(rawFile)

	hash := generateSignatureHash(strFile, usr.ID, comp.CompanyID, comp.PrivateKey)

	transaction := document.Transaction{
		CompanyID:  usr.Info.CompanyID,
		DocumentID: doc.ID,
		Signature: document.Signature{
			UserID:    usr.ID,
			Hash:      hash,
			Timestamp: time.Now(),
		},
	}

	var update document.UpdateInfo

	updTransactions := doc.Info.Transactions
	updTransactions = append(updTransactions, transaction)

	update.Transactions = updTransactions

	if len(update.Transactions) == len(req.Receivers)+1 {
		var signHash [][]byte

		for _, t := range update.Transactions {
			signHash = append(signHash, t.Signature.Hash)
		}

		resHash := bytes.Join(
			signHash,
			[]byte{},
		)

		h := sha256.Sum256(resHash)

		str := encoder.EncodeToString(h[:])
		update.SignatureHash = &str

		if err = s.documentService.UpdateInfo(ctx, &update, doc.ID); err != nil {
			return err
		}

		requestID, err := primitive.ObjectIDFromHex(req.ID)
		if err != nil {
			return err
		}

		if err := s.repo.Update(ctx, &signrequest.Model{
			ID:     requestID,
			Status: signrequest.Completed,
		}); err != nil {
			return err
		}

		updDoc, err := s.documentService.FetchOne(ctx, doc.ID, usr)
		if err != nil {
			return err
		}

		cert, err := s.certService.Create(ctx, updDoc)
		if err != nil {
			return err
		}

		certID := primitive.NewObjectID()

		if err := s.fileRepo.Create(ctx, doc.Name, cert, certID); err != nil {
			return err
		}

		if err := s.documentService.UpdateCertID(ctx, doc.ID, certID.Hex()); err != nil {
			return err
		}

	} else {
		if err = s.documentService.UpdateInfo(ctx, &update, doc.ID); err != nil {
			return err
		}
	}

	return nil
}

func appendWithoutDuplicates(arr []string, el string) []string {
	var res []string
	for _, v := range arr {
		if v != el {
			res = append(res, v)
		}
	}

	res = append(res, el)

	return res
}

func (s *SignRequestService) Delete(ctx context.Context, id string, usr *user.User) error {
	requestID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return ErrInvalidSignRequestID
	}
	req, err := s.repo.FetchOne(ctx, bson.D{{"_id", requestID}})
	if err != nil {
		return ErrSignRequestNotFound
	}

	doc, err := s.documentService.FetchOne(ctx, id, usr)
	if err != nil {
		return err
	}

	if req.SenderID != usr.ID {
		return ErrForbidden
	}

	if req.Status != signrequest.Declined {
		return errors.New("only declined requests can be deleted")
	}

	if err := s.documentService.DeleteInfo(ctx, doc.ID); err != nil {
		return err
	}

	//TODO: WRITE BETTER ERROR HANDLING
	if err = s.repo.Delete(ctx, bson.D{{"_id", requestID}}); err != nil {
		return err
	}

	return nil
}

func generateSignatureHash(file string, userID string, companyID string, pkey []byte) []byte {
	//Hmacing
	hash := hmac.New(sha256.New, pkey)
	hash.Write([]byte(userID + "." + file + "." + companyID))
	return hash.Sum(nil)
}

func verifySignature(transactions []document.Transaction, userID string) bool {
	//Checking for transaction whether the user already signed the document
	for _, t := range transactions {
		if t.Signature.UserID == userID {
			return true
		}
	}

	return false
}

func checkUser(rec []string, id string) bool {
	for _, v := range rec {
		if v == id {
			return true
		}
	}

	return false
}
