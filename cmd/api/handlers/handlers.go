package handlers

import (
	"net/http"
	"os"

	exmatic_org "gitlab.com/exmatic/doc/internal/repository/exmatic-org"
	custom_swagger "gitlab.com/exmatic/doc/pkg/custom-swagger"

	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/gridfs"
	"go.uber.org/zap"

	"gitlab.com/exmatic/doc/cmd/api/handlers/v1/companyinfogrp"
	"gitlab.com/exmatic/doc/cmd/api/handlers/v1/documentgrp"
	"gitlab.com/exmatic/doc/cmd/api/handlers/v1/signrequestgrp"
	_ "gitlab.com/exmatic/doc/docs" // init call
	"gitlab.com/exmatic/doc/internal/core/role"
	"gitlab.com/exmatic/doc/internal/core/user"
	exmatic_auth "gitlab.com/exmatic/doc/internal/repository/exmatic-auth"
	"gitlab.com/exmatic/doc/internal/repository/mongodb"
	"gitlab.com/exmatic/doc/internal/service"
	"gitlab.com/exmatic/doc/internal/web/v1/middleware"
	"gitlab.com/exmatic/doc/pkg/sugar"
	"gitlab.com/exmatic/doc/pkg/web"
)

var (
	version = "v1"
)

func API(shutdown chan os.Signal, log *zap.SugaredLogger, db *mongo.Database, bucket *gridfs.Bucket, auth user.Auth, keyService *sugar.Sugar) *web.App {
	app := web.NewApp(shutdown, middleware.Logger(log), middleware.Errors(log), middleware.Panics())
	authen := middleware.Authenticate(auth)
	compauth := middleware.Authorize(role.Admin, role.HR, role.Employee)

	// swagger
	swaggerHandler := custom_swagger.Handler()
	app.Handle(http.MethodGet, version, "/swagger/*", swaggerHandler)

	companyInfoRepo := mongodb.NewCompanyInfoRepository(db, log)
	companyInfoService := service.NewCompanyInfoService(companyInfoRepo, keyService)
	companyInfoHandler := companyinfogrp.NewHandler(companyInfoService)

	app.Handle(http.MethodPost, version, "/company/info", companyInfoHandler.Create)
	app.Handle(http.MethodGet, version, "/company/info/:id", companyInfoHandler.FetchOne, authen, compauth)

	userRepo := exmatic_auth.NewUserRepository(&http.Client{})
	fileRepo := mongodb.NewFileRepository(bucket)
	documentRepo := mongodb.NewDocumentRepository(db)
	documentService := service.NewDocumentService(documentRepo, fileRepo, userRepo)
	docHandler := documentgrp.NewHandler(documentService)

	app.Handle(http.MethodPost, version, "/document", docHandler.Create, authen, compauth)
	app.Handle(http.MethodGet, version, "/document/:id", docHandler.GetOneByID, authen, compauth)
	app.Handle(http.MethodGet, version, "/documents", docHandler.Get, authen, compauth)
	app.Handle(http.MethodGet, version, "/document/raw/:id", docHandler.GetRawDocument, authen, compauth)
	app.Handle(http.MethodPut, version, "/document/:id", docHandler.Update, authen, compauth)
	app.Handle(http.MethodDelete, version, "/document/:id", docHandler.Delete, authen, compauth)

	signRepo := mongodb.NewSignRequestRepository(db, log)
	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	compRepo := exmatic_org.NewCompanyRepository()
	certService := service.NewCertificateService(m, userRepo, compRepo)
	signService := service.NewSignRequestService(signRepo, companyInfoService, documentService, fileRepo, userRepo, certService)
	signHandler := signrequestgrp.NewHandler(signService)
	app.Handle(http.MethodPost, version, "/sign-request", signHandler.Create, authen, compauth)
	app.Handle(http.MethodGet, version, "/sign-requests", signHandler.Get, authen, compauth)
	app.Handle(http.MethodGet, version, "/sign-requests/:id", signHandler.GetOneByID, authen, compauth)
	app.Handle(http.MethodPost, version, "/sign-request/sign/:id", signHandler.Sign, authen, compauth)
	app.Handle(http.MethodPost, version, "/sign-request/decline/:id", signHandler.Decline, authen, compauth)
	app.Handle(http.MethodDelete, version, "/sign-request/delete/:id", signHandler.Delete, authen, compauth)

	return app
}
