package documentgrp

import (
	"context"
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/doc/internal/core/document"
	"gitlab.com/exmatic/doc/internal/web/v1"
	"gitlab.com/exmatic/doc/pkg/web"
)

type Handler struct {
	service document.Service
}

func NewHandler(service document.Service) *Handler {
	return &Handler{
		service: service,
	}
}

// @Summary Create document
// @Security ApiKeyAuth
// @Tags document
// @Description adds a new document to the system
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param input body document.New true "Supported type values: DOC_TYPE_CONTRACT, DOC_TYPE_CV, DOC_TYPE_MEDICAL_RECORD, DOC_TYPE_DIPLOMA, DOC_TYPE_MILITARY_RECORD, DOC_TYPE_PERSONAL_ID"
// @Success 201 {integer} integer
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /document [post]
func (h *Handler) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	var form document.New
	if usr.Info == nil {
		return v1.NewRequestError(errors.New("you should be in a company to create a document"), http.StatusForbidden)
	}

	if err := web.Decode(r, &form); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	if err := h.service.Create(ctx, &form, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusCreated)
}

// @Summary Delete document
// @Security ApiKeyAuth
// @Tags document
// @Description removes a document from the system
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "document ID"
// @Success 200 {integer} integer "httpStatusOK"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /document/{id} [delete]
func (h *Handler) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	if err := h.service.Delete(ctx, id, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// Update PUT v1/document/:id
/*
	{
	"name" : "",
	"type" : "",
 	"description" : "",
	}
*/

// @Summary Update document
// @Security ApiKeyAuth
// @Tags document
// @Description Update updates a document in the system
// @Description If document type = DOC_TYPE_CONTRACT and someone already signed it, then you can't update it.
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "document ID"
// @Param input body document.Update true "document info"
// @Success 200 {integer} integer "httpStatusOK"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /document/{id} [put]
func (h *Handler) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	var update document.Update
	if err := web.Decode(r, &update); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	if err := h.service.Update(ctx, &update, id, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary Get documents
// @Security ApiKeyAuth
// @Tags document
// @Description returns a list of documents with filtering
// @Description Roles required: Employee/HR/Admin
// @Description example of a query with filters:
// @Description localhost:8081/v1/documents?id={id}&type={type}&user_id={user_id}&outer={true/false}
// @Accept  json
// @Produce json
// @Param id query string false "document ID"
// @Param type query string false "document type"
// @Param user_id query string false "user ID"
// @Param outer query boolean false "false - show only those documents that you created. true - those documents that were assigned to you for sign"
// @Success 200 {array} document.Document "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /documents [get]
func (h *Handler) Get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	data, err := h.service.Fetch(ctx, filter(r), usr)
	if err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, data, http.StatusOK)
}

// @Summary GetOneByID document
// @Security ApiKeyAuth
// @Tags document
// @Description returns a document by its ID
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "document ID"
// @Success 200 {object} document.Document "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /document/{id} [get]
func (h *Handler) GetOneByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	data, err := h.service.FetchOne(ctx, id, usr)
	if err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, data, http.StatusOK)
}

// @Summary GetRawDocument document
// @Security ApiKeyAuth
// @Tags document
// @Description returns a raw document by its ID which contains id, data and certificate of the document
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "document ID"
// @Success 200 {object} file.File "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /document/raw/{id} [get]
func (h *Handler) GetRawDocument(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	data, err := h.service.FetchRaw(ctx, id, usr)
	if err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, data, http.StatusOK)
}

func filter(r *http.Request) document.Filter {
	queryMap := r.URL.Query()
	var outer bool
	innerArr := queryMap["outer"]
	if innerArr != nil {
		if strings.ToUpper(innerArr[0]) == "TRUE" {
			outer = true
		}
	}

	res := document.Filter{
		IDs:   queryMap["id"],
		Types: queryMap["type"],
		Users: queryMap["user_id"],
		Outer: outer,
	}

	return res
}
