package companyinfogrp

import (
	"context"
	"net/http"

	"gitlab.com/exmatic/doc/internal/core/companyinfo"
	"gitlab.com/exmatic/doc/internal/web/v1"
	"gitlab.com/exmatic/doc/pkg/web"
)

type Handler struct {
	service companyinfo.Service
}

func NewHandler(service companyinfo.Service) *Handler {
	return &Handler{
		service: service,
	}
}

func (h *Handler) Handler(w http.ResponseWriter, r *http.Request) {

}

// @Summary Create companyinfo
// @Tags companyinfo
// @Description creates company's private key by id and stores it.
// @Description private_key field NOT required to be set, just skip it.
// @Description Roles required: Admin
// @Description Note: this endpoint ideally should be hidden from clients and called automatically ONLY on the backend by org microservice after company creation. Currently available for tests.
// @Accept  json
// @Produce json
// @Param input body companyinfo.CompanyInfo true "company id"
// @Success 201 {integer} integer "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /company/info [post]
func (h *Handler) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {

	var info companyinfo.CompanyInfo
	if err := web.Decode(r, &info); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	if err := h.service.Create(ctx, &info); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusCreated)
}

// @Summary FetchOne companyinfo
// @Security ApiKeyAuth
// @Tags companyinfo
// @Description returns a companyinfo by companyinfoID.
// @Description Roles required: Employee/HR/Admin
// @Description Note: this endpoint ideally should be hidden from clients and called ONLY by the backend. Currently available for tests.
// @Accept  json
// @Produce json
// @Param id path string true "companyinfo ID"
// @Success 200 {object} companyinfo.Raw "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /company/info/{id} [get]
func (h *Handler) FetchOne(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	compID := web.Param(r, "id")

	raw, err := h.service.FetchOne(ctx, compID)
	if err != nil {
		return web.Respond(ctx, w, nil, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, raw, http.StatusOK)
}
