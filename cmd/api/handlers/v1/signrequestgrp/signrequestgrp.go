package signrequestgrp

import (
	"context"
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/exmatic/doc/internal/core/signrequest"
	"gitlab.com/exmatic/doc/internal/web/v1"
	"gitlab.com/exmatic/doc/pkg/web"
)

type Handler struct {
	service signrequest.Service
}

func NewHandler(service signrequest.Service) *Handler {
	return &Handler{
		service: service,
	}
}

// @Summary Create sign-request
// @Security ApiKeyAuth
// @Tags sign-request
// @Description adds a new sign-request to the system
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param input body signrequest.New true "Supported type values: REQUEST_TYPE_INNER, REQUEST_TYPE_OUTER"
// @Success 201 {integer} integer
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /sign-request [post]
func (h *Handler) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}
	var new signrequest.New
	if err := web.Decode(r, &new); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	if err := h.service.Create(ctx, &new, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusCreated)
}

// @Summary Sign sign-request
// @Security ApiKeyAuth
// @Tags sign-request
// @Description signs a sign-request
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "sign-request ID"
// @Success 200 {integer} integer
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /sign-request/sign/{id} [post]
func (h *Handler) Sign(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	if err := h.service.Sign(ctx, id, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary Decline sign-request
// @Security ApiKeyAuth
// @Tags sign-request
// @Description declines a sign-request
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "sign-request ID"
// @Success 200 {integer} integer
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse
// @Failure default {object} v1.ErrorResponse
// @Router /sign-request/decline/{id} [post]
func (h *Handler) Decline(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	if err := h.service.Decline(ctx, id, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

// @Summary Get sign-requests
// @Security ApiKeyAuth
// @Tags sign-request
// @Description returns a list of sign-requests with filtering
// @Description Roles required: Employee/HR/Admin
// @Description example of a query with filters:
// @Description localhost:8081/v1/sign-requests?id={id}&status={status}&outer={true/false}
// @Accept  json
// @Produce json
// @Param id query string false "sign-request ID"
// @Param status query string false "sign-request status, supported values: STATUS_DECLINED, STATUS_COMPLETED, STATUS_IN_PROCESS, STATUS_DOCUMENT_DELETED"
// @Param outer query boolean false "false - show only those sign-requests that you created. true - those requests that were assigned to you for sign"
// @Success 200 {array} signrequest.SignRequest "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /sign-requests [get]
func (h *Handler) Get(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	data, err := h.service.Fetch(ctx, filter(r), usr)
	if err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, data, http.StatusOK)
}

// @Summary GetOneByID sign-request
// @Security ApiKeyAuth
// @Tags sign-request
// @Description returns a sign-request by its ID
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "sign-request ID"
// @Success 200 {object} signrequest.SignRequest "httpStatusOk"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /sign-requests/{id} [get]
func (h *Handler) GetOneByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	req, err := h.service.FetchOneByID(ctx, id, usr)
	if err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, req, http.StatusOK)
}

func filter(r *http.Request) *signrequest.Filter {
	queryMap := r.URL.Query()
	var outer bool
	innerArr := queryMap["outer"]
	if innerArr != nil {
		if strings.ToUpper(innerArr[0]) == "TRUE" {
			outer = true
		}
	}

	res := &signrequest.Filter{
		IDs:      queryMap["id"],
		Statuses: queryMap["status"],
		Outer:    outer,
	}

	return res
}

// @Summary Delete sign-request
// @Security ApiKeyAuth
// @Tags sign-request
// @Description removes a sign-request from the system
// @Description Roles required: Employee/HR/Admin
// @Accept  json
// @Produce json
// @Param id path string true "sign-request ID"
// @Success 200 {integer} integer "httpStatusOK"
// @Failure 400 {object} v1.ErrorResponse "httpStatusBadRequest"
// @Failure 500 {object} v1.ErrorResponse "httpStatusInternalServerError"
// @Failure default {object} v1.ErrorResponse
// @Router /sign-request/delete/{id} [delete]
func (h *Handler) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	usr, err := v1.UserFromContext(ctx)
	if err != nil {
		return err
	}

	id := web.Param(r, "id")
	if id == "" {
		return v1.NewRequestError(errors.New("invalid id"), http.StatusBadRequest)
	}

	if err := h.service.Delete(ctx, id, usr); err != nil {
		return v1.NewRequestError(err, http.StatusBadRequest)
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}
